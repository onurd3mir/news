﻿namespace News.Domain.ElasticSearch;

public class ElasticSearchInsertModel : ElasticSearchModel
{
    public object? Item { get; set; }
}
