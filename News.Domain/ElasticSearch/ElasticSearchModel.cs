﻿namespace News.Domain.ElasticSearch;

public class ElasticSearchModel
{
    public string? IndexName { get; set; }

    public string? AliasName { get; set; }
}
