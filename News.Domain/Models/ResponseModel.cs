﻿namespace News.Domain.Models;

public class ResponseModel<T>
{
    public T? Result;
    public bool Status;
    public string? Message;
    public List<string>? Errors;

    public static ResponseModel<T> Success(T result, string message)
    {
        return new ResponseModel<T>
        {
            Result = result,
            Status = true,
            Message = message,
        };
    }

    public static ResponseModel<T> Success(string message)
    {
        return new ResponseModel<T>
        {
            Result = default,
            Status = true,
            Message = message,
        };
    }

    public static ResponseModel<T> Fail(T result, string message, List<string>? errors = null)
    {
        return new ResponseModel<T>
        {
            Result = result,
            Status = false,
            Message = message,
            Errors = errors,
        };
    }

    public static ResponseModel<T> Fail(string message, List<string>? errors = null)
    {
        return new ResponseModel<T>
        {
            Result = default,
            Status = false,
            Message = message,
            Errors = errors
        };
    }
}