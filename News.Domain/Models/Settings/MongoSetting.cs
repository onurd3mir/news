﻿namespace News.Domain.Models.Settings;

public class MongoSetting
{
    public string Server { get; set; }
    public string Database { get; set; }
}
