﻿namespace News.Domain.Models.Settings;

public class RedisSetting
{
    public string? Server { get; set; }
    public int Port { get; set; }
    public bool AllowAdmin { get; set; }
    public int ConnectTimeout { get; set; }
    public int Database { get; set; }
}