﻿namespace News.Domain.Models.Settings;

public class RabbitMqSetting
{
    public string Server { get; set; }
    public int Port { get; set; }
}
