﻿namespace News.Domain;

public class Costants
{
    public static class RabbitMq
    {
        // Exchange
        public const string CreateNewsExchange = "create.news.exchange";

        //RoutingKey
        public const string CreateNews = "createnews";
    }

    public static class ElasticSearch
    {
        public const string NewsApiIndexName = "newsapi_news";

        public const string NewsApiIndexAliasName = "news";
    }
}
