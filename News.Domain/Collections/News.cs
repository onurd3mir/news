﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson;

namespace News.Domain.Collections;

public class News
{
    [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Source { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string UrlAddress { get; set; }
    public string UrlToImageAddress { get; set; }
    public DateTime? PublishedAt { get; set; }
    public string Content { get; set; }
    public string LangCode { get; set; }
    
    public Category Category { get; set; }
}