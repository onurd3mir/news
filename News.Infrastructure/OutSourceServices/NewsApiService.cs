﻿using News.Domain.Models.OutSources;
using News.Infrastructure.OutSourceServices.Abstracts;
using NewsAPI;
using NewsAPI.Constants;
using NewsAPI.Models;

namespace News.Infrastructure.OutSourceServices;

public class NewsApiService : INewsService
{
    public async Task<NewsResponseDto?> GetNews(string country, string category, string apiKey)
    {
        var newsApiClient = new NewsApiClient(apiKey);

        var articlesResponse = await newsApiClient.GetTopHeadlinesAsync(new TopHeadlinesRequest
        {
            Country = GetCountries(country),
            Category = GetCategories(category),
            Page = 1,
            PageSize = 100
        });

        if (articlesResponse.Status == Statuses.Error)
            return new NewsResponseDto { Status = "Error" };

        var result = new NewsResponseDto
        {
            Status = "Success",
            TotalResults = articlesResponse.TotalResults,
            Articles = new List<NewsResponseDto.Article>()
        };

        foreach (var article in articlesResponse.Articles)
        {
            result.Articles.Add(new NewsResponseDto.Article
            {
                Title = article.Title,
                Content = article.Content,
                Description = article.Description,
                PublishedAt = article.PublishedAt,
                Url = article.Url,
                UrlToImage = article.UrlToImage,
                Author = article.Author,
                Source = new NewsResponseDto.Source
                {
                    Id = article.Source.Id,
                    Name = article.Source.Name
                },
            });
        }

        return result;
    }

    private static Countries GetCountries(string country)
    {
        return country switch
        {
            "tr" => Countries.TR,
            "de" => Countries.DE,
            "gb" => Countries.GB,
            "ru" => Countries.RU,
            "fr" => Countries.FR,
            "ua" => Countries.UA,
            _ => Countries.TR
        };
    }

    private static Categories GetCategories(string category)
    {
        return category switch
        {
            "Spor" => Categories.Sports,
            "İş" => Categories.Business,
            "Magazin" => Categories.Entertainment,
            "Sağlık" => Categories.Health,
            "Bilim" => Categories.Science,
            "Teknoloji" => Categories.Technology,
            _ => Categories.Technology
        };
    }
}