﻿using News.Domain.Models.OutSources;

namespace News.Infrastructure.OutSourceServices.Abstracts;

public interface INewsService
{
    Task<NewsResponseDto?> GetNews(string country, string category, string apiKey);
}