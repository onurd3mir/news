﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using News.Domain.Models.Settings;
using News.Infrastructure.CrossCuttingConcerns.Caching;
using News.Infrastructure.CrossCuttingConcerns.Searching;
using News.Infrastructure.OutSourceServices;
using News.Infrastructure.OutSourceServices.Abstracts;
using News.Infrastructure.Persistence.Repositories;
using News.Infrastructure.Persistence.Repositories.Abstracts;
using StackExchange.Redis;

namespace News.Infrastructure;

public static class InfrastructureServiceRegistration
{
    public static void AddInfrastructureService(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddStackExchangeRedisCache(redisOptions =>
        {
            var redisSetting = configuration.GetSection(nameof(RedisSetting)).Get<RedisSetting>();
            redisOptions.ConfigurationOptions = new ConfigurationOptions
            {
                EndPoints = { { redisSetting!.Server, redisSetting.Port } },
                AllowAdmin = redisSetting.AllowAdmin,
                ConnectTimeout = redisSetting.ConnectTimeout,
                DefaultDatabase = redisSetting.Database
            };
        });
        services.AddSingleton<ICacheService, RedisService>();

        services.AddSingleton<IElasticSearch, ElasticSearchManager>();

        services.AddScoped<INewsService, NewsApiService>();

        services.AddScoped<ICategoryRepository, CategoryRepository>()
            .AddScoped<INewsRepository, NewsRepository>()
            .AddScoped<ILanguageRepository, LanguageRepository>();
    }
}