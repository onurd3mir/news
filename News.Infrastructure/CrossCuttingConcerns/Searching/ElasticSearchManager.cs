﻿using Microsoft.Extensions.Configuration;
using Nest;
using News.Domain.ElasticSearch;
using News.Domain.Models;
using News.Domain.Models.Settings;

namespace News.Infrastructure.CrossCuttingConcerns.Searching;

public class ElasticSearchManager : IElasticSearch
{
    private readonly ElasticClient _client;
    public ElasticSearchManager(IConfiguration configuration)
    {
        var eleasticSearchSetting = configuration.GetSection(nameof(ElasticSearchSetting)).Get<ElasticSearchSetting>();
        var settings = new ConnectionSettings(new Uri($"{eleasticSearchSetting!.Server}:{eleasticSearchSetting!.Port}/"));
        _client = new ElasticClient(settings);
    }

    public IElasticClient Client => _client;

    public async Task<ResponseModel<bool>> CreateIndexAsync(ElasticSearchModel model)
    {
        var indexExists = await _client.Indices.ExistsAsync(model.IndexName);

        if (indexExists.Exists)
            return ResponseModel<bool>.Success(indexExists.Exists, "İndex Mevcut");


        var index = await _client.Indices.CreateAsync(model.IndexName, opt =>
                                               opt.Aliases(x => x.Alias(model.AliasName)));

        if (index.IsValid)
            return ResponseModel<bool>.Success(index.IsValid, "İşlem Başarılı");

        return ResponseModel<bool>.Fail(index.IsValid, "İşlem Başarısız");
    }

    public async Task<ResponseModel<bool>> InsertAsync(ElasticSearchInsertModel model)
    {
        var insert = await _client.IndexAsync(model.Item, i => i.Index(model.IndexName));

        if (insert.IsValid)
            return ResponseModel<bool>.Success(insert.IsValid, "İşlem Başarılı");

        return ResponseModel<bool>.Fail(insert.IsValid, "İşlem Başarısız");
    }
}
