﻿using Nest;
using News.Domain.ElasticSearch;
using News.Domain.Models;

namespace News.Infrastructure.CrossCuttingConcerns.Searching;

public interface IElasticSearch
{
    IElasticClient Client { get; }

    Task<ResponseModel<bool>> CreateIndexAsync(ElasticSearchModel model);
    Task<ResponseModel<bool>> InsertAsync(ElasticSearchInsertModel model);
}
