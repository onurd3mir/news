﻿using System.Text.Json;
using Microsoft.Extensions.Caching.Distributed;

namespace News.Infrastructure.CrossCuttingConcerns.Caching;

public class RedisService : ICacheService
{
    private readonly IDistributedCache _distributedCache;

    public RedisService(IDistributedCache distributedCache)
    {
        _distributedCache = distributedCache;
    }

    public T Get<T>(string key, int duration, Func<T> acquire)
    {
        var (isSet, item) = TryGetItem<T>(key);

        if (isSet)
            return item;

        var result = acquire();

        if (result != null)
            Set(key, result, duration);

        return result;
    }

    public async Task<T> GetAsync<T>(string key, int duration, Func<T> acquire)
    {
        var (isSet, item) = await TryGetItemAsync<T>(key);

        if (isSet)
            return item;

        var result = acquire();

        if (result != null)
            Set(key, result, duration);

        return result;
    }

    public void Set(string key, object data, int duration)
    {
        _distributedCache.SetString(key, JsonSerializer.Serialize(data), new DistributedCacheEntryOptions
        {
            AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(duration)
        });
    }

    public bool IsExist(string key)
    {
        return _distributedCache.Get(key) != null;
    }

    public void Remove(string key)
    {
        _distributedCache.Remove(key);
    }

    public void RemoveByPattern(string pattern)
    {
        throw new NotImplementedException();
    }

    private async Task<(bool, T? item)> TryGetItemAsync<T>(string key)
    {
        var json = await _distributedCache.GetStringAsync(key);

        if (string.IsNullOrEmpty(json))
            return (false, default);

        var item = JsonSerializer.Deserialize<T>(json);

        return (true, item);
    }

    private (bool, T? item) TryGetItem<T>(string key)
    {
        var json = _distributedCache.GetString(key);

        if (string.IsNullOrEmpty(json))
            return (false, default);

        var item = JsonSerializer.Deserialize<T>(json);

        return (true, item);
    }
}