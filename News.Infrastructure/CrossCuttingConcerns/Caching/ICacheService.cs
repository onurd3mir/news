﻿namespace News.Infrastructure.CrossCuttingConcerns.Caching;

public interface ICacheService
{
    T Get<T>(string key, int duration, Func<T> acquire);
    Task<T> GetAsync<T>(string key, int duration, Func<T> acquire);
    void Set(string key, object data, int duration);
    bool IsExist(string key);
    void Remove(string key);
    void RemoveByPattern(string pattern);
}