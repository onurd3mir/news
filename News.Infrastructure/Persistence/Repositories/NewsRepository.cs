﻿using System.Linq.Expressions;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using News.Infrastructure.Persistence.Repositories.Abstracts;

namespace News.Infrastructure.Persistence.Repositories;

public class NewsRepository : BaseMongoRepository<Domain.Collections.News>, INewsRepository
{
    public NewsRepository(IConfiguration configuration) : base(configuration, "news")
    {
    }

    public Task<List<Domain.Collections.News>> GetFilterAsync(Expression<Func<Domain.Collections.News, bool>> predicate,
        int index = 1, int size = 10)
    {
        return _collection.Find(predicate)
            .Skip((index - 1) * size)
            .Limit(size)
            .SortByDescending(x => x.PublishedAt)
            .ToListAsync();
    }

    public async Task<Domain.Collections.News> GetByIdAsync(string id)
    {
        return await FindByAsync(x => x.Id == id);
    }

    public async Task AddAsync(Domain.Collections.News collection)
    {
        await InsertAsync(collection);
    }

    public async Task<bool> AnyByUrl(string url)
    {
        var result = await FindByAsync(f => f.UrlAddress == url);

        return result is not null;
    }

    public async Task<List<Domain.Collections.News>> GetAllAsync()
    {
        return await FindAllAsync();
    }

    public async Task<List<Domain.Collections.News>> GetByCategoryAsync(string categoryId, string languageCode,
        int index = 1, int size = 10)
    {
        return await _collection.Find(p => p.Category.Id == categoryId && p.LangCode == languageCode)
            .Skip((index - 1) * size)
            .Limit(size)
            .SortByDescending(p => p.PublishedAt)
            .ToListAsync();
    }
}