﻿using News.Domain.Collections;

namespace News.Infrastructure.Persistence.Repositories.Abstracts;

public interface ICategoryRepository
{
    Task<List<Category>> GetAllAsync();

    Task AddAsync(Category category);
}