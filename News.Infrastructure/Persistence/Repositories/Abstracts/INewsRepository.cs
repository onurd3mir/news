﻿using System.Linq.Expressions;

namespace News.Infrastructure.Persistence.Repositories.Abstracts;

public interface INewsRepository
{
    Task<List<Domain.Collections.News>> GetAllAsync();

    Task<List<Domain.Collections.News>> GetFilterAsync(Expression<Func<Domain.Collections.News, bool>> predicate,
        int index = 1, int size = 10);

    Task<Domain.Collections.News> GetByIdAsync(string id);
    Task<List<Domain.Collections.News>> GetByCategoryAsync(string categoryId,string languageCode, int index = 1, int size = 10);
    Task AddAsync(Domain.Collections.News collection);
    Task<bool> AnyByUrl(string url);
}