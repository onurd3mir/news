﻿using MongoDB.Driver;
using System.Linq.Expressions;
using Microsoft.Extensions.Configuration;
using News.Domain.Models.Settings;

namespace News.Infrastructure.Persistence.Repositories.Abstracts;

public abstract class BaseMongoRepository<T>
{
    protected readonly IMongoCollection<T> _collection;

    protected BaseMongoRepository(IConfiguration configuration, string collection)
    {
        var mongoSetting = configuration.GetSection(nameof(MongoSetting)).Get<MongoSetting>();
        var client = new MongoClient(mongoSetting!.Server);
        var database = client.GetDatabase(mongoSetting.Database);
        _collection = database.GetCollection<T>(collection);
    }

    protected IQueryable<T> Query()
    {
        return _collection.AsQueryable();
    }

    protected async Task<List<T>> FindAllAsync()
    {
        return await _collection.AsQueryable().ToListAsync();
    }

    protected async Task<List<T>> FindAsync(Expression<Func<T, bool>> predicate)
    {
        return await _collection.Find(predicate).ToListAsync();
    }

    protected async Task<T> FindByAsync(Expression<Func<T, bool>> predicate)
    {
        return await _collection.Find(predicate).FirstOrDefaultAsync();
    }

    protected async Task InsertAsync(T entity)
    {
        await _collection.InsertOneAsync(entity);
    }

    protected async Task InsertManyAsync(IEnumerable<T> entityList)
    {
        await _collection.InsertManyAsync(entityList);
    }

    protected async Task DeleteAsync(Expression<Func<T, bool>> predicate)
    {
        await _collection.DeleteOneAsync(predicate);
    }

    protected async Task DeleteManyAsync(Expression<Func<T, bool>> predicate)
    {
        await _collection.DeleteManyAsync(predicate);
    }

    protected async Task UpdateAsync(Expression<Func<T, bool>> predicate, T entity)
    {
        await _collection.FindOneAndReplaceAsync(predicate, entity);
    }
}