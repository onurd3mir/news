﻿using News.Domain.Collections;

namespace News.Infrastructure.Persistence.Repositories.Abstracts;

public interface ILanguageRepository
{
    Task<List<Language>> GetAllAsync();

    Task<List<Language>> GetListQueryAsync(bool isActive);

    Task AddAsync(Language language);
}