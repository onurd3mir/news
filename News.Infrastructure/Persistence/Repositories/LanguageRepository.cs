﻿using Microsoft.Extensions.Configuration;
using News.Domain.Collections;
using News.Infrastructure.Persistence.Repositories.Abstracts;

namespace News.Infrastructure.Persistence.Repositories;

public class LanguageRepository : BaseMongoRepository<Language>, ILanguageRepository
{
    public LanguageRepository(IConfiguration configuration) : base(configuration, "languages")
    {
    }

    public async Task<List<Language>> GetAllAsync()
    {
        return await FindAllAsync();
    }

    public async Task<List<Language>> GetListQueryAsync(bool isActive)
    {
        return await FindAsync(x => x.IsActive == isActive);
    }

    public async Task AddAsync(Language language)
    {
        await InsertAsync(language);
    }
}