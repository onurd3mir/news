﻿using Microsoft.Extensions.Configuration;
using News.Domain.Collections;
using News.Infrastructure.Persistence.Repositories.Abstracts;

namespace News.Infrastructure.Persistence.Repositories;

public class CategoryRepository : BaseMongoRepository<Category>, ICategoryRepository
{
    public CategoryRepository(IConfiguration configuration) : base(configuration, "categories")
    {
    }

    public async Task<List<Category>> GetAllAsync()
    {
        return await FindAllAsync();
    }

    public async Task AddAsync(Category category)
    {
        await base.InsertAsync(category);
    }
}