﻿using News.WebUI.Services.Abstracts;

namespace News.WebUI.Handler;

public class ClientTokenHandler : DelegatingHandler
{
    private readonly IClientTokenService _clientTokenService;

    public ClientTokenHandler(IClientTokenService clientTokenService)
    {
        _clientTokenService = clientTokenService;
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
        CancellationToken cancellationToken)
    {
        request.Headers.Authorization =
            new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", await _clientTokenService.GetToken());

        var response = await base.SendAsync(request, cancellationToken);

        if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
        {
            throw new UnauthorizedAccessException();
        }

        return response;
    }
}