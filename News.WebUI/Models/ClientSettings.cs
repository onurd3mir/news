﻿namespace News.WebUI.Models;

public class ClientSettings
{
    public string? Email { get; set; }

    public string? Password { get; set; }
}