﻿namespace News.WebUI.Models;

public class ServiceApiSettings
{
    public string? GatewayBaseUri { get; set; }
    public ServiceApi? News { get; set; }

    public class ServiceApi
    {
        public string? Endpoint { get; set; }
    }
}
