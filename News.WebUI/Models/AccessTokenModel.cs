﻿namespace News.WebUI.Models;

public class AccessTokenModel
{
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
    public DateTime Expiration { get; set; }
}