﻿namespace News.WebUI.Models.Sevices.News;

public class LanguageListModel
{
    public string Code { get; set; }
    
    public string Name { get; set; }
}