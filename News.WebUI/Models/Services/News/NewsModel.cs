﻿namespace News.WebUI.Models.Sevices.News;

public class NewsModel
{
    public string NewsId { get; set; }
    public string Source { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string UrlAddress { get; set; }
    public string UrlToImageAddress { get; set; }
    public DateTime? PublishedAt { get; set; }
    public string Content { get; set; }
    public string LangCode { get; set; }
    public string CategoryName { get; set; }
}
