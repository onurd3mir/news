﻿namespace News.WebUI.Models.Sevices.News;

public class CategoryListModel
{
    public string CategoryId { get; set; }
    public string CategoryName { get; set; }
}
