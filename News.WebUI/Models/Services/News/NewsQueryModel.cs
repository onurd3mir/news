﻿namespace News.WebUI.Models.Sevices.News;

public class NewsQueryModel
{
    public string LanguageCode { get; set; }
    public string CategoryId { get; set; } = string.Empty;
    public string SearchKey { get; set; } = string.Empty;
    public int Page { get; set; } = 1;
    public int PageSize { get; set; } = 10;
}
