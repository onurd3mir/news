﻿using News.WebUI.Models.Sevices.News;

namespace News.WebUI.Models.Home;

public class HomeViewModel
{
    public HomeViewModel()
    {
        BusinessNews = new List<NewsListModel>();
        SportNews = new List<NewsListModel>();
        MagazineNews = new List<NewsListModel>();
        HealthNews = new List<NewsListModel>();
        TechnologyNews = new List<NewsListModel>();
        ScienceNews = new List<NewsListModel>();
    }
    public List<NewsListModel> BusinessNews { get; set; }
    public List<NewsListModel> SportNews { get; set; }
    public List<NewsListModel> MagazineNews { get; set; }
    public List<NewsListModel> HealthNews { get; set; }
    public List<NewsListModel> ScienceNews { get; set; }
    public List<NewsListModel> TechnologyNews { get; set; }
}
