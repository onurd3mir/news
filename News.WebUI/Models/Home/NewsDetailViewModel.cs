﻿using News.WebUI.Models.Sevices.News;

namespace News.WebUI.Models.Home;

public class NewsDetailViewModel
{
    public NewsModel NewsDetail { get; set; }
    
    public List<NewsListModel> NewsItems { get; set; }
}