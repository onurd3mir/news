﻿using News.WebUI.Handler;
using News.WebUI.Models;
using News.WebUI.Services;
using News.WebUI.Services.Abstracts;

namespace News.WebUI.Extensions;

public static class ServiceExtension
{
    public static void AddHttpClientServices(this IServiceCollection services, IConfiguration Configuration)
    {
        var serviceApiSettigns = Configuration.GetSection("ServiceApiSettings").Get<ServiceApiSettings>();

        services.AddHttpClient<IClientTokenService, ClientTokenService>(opt =>
        {
            opt.BaseAddress = new Uri($"{serviceApiSettigns?.GatewayBaseUri}/{serviceApiSettigns?.News?.Endpoint}");
        });

        services.AddHttpClient<INewsService, NewsService>(opt =>
        {
            opt.BaseAddress = new Uri($"{serviceApiSettigns?.GatewayBaseUri}/{serviceApiSettigns?.News?.Endpoint}");
        }).AddHttpMessageHandler<ClientTokenHandler>();
    }
}
