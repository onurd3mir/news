﻿namespace News.WebUI.Extensions;

public class LanguageMiddleware
{
    private readonly RequestDelegate _next;

    public LanguageMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        const string cookieName = "langcode";

        if (httpContext.Request.Cookies[cookieName] is null)
        {
            var cookieOptions = new CookieOptions
            {
                Expires = DateTime.Now.AddDays(1)
            };
            httpContext.Response.Cookies.Append(cookieName, "tr", cookieOptions);
        }

        await _next.Invoke(httpContext);
    }
}