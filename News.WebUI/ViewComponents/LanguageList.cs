﻿using Microsoft.AspNetCore.Mvc;
using News.WebUI.Services.Abstracts;

namespace News.WebUI.ViewComponents;

public class LanguageList : ViewComponent
{
    private readonly INewsService _newsService;

    public LanguageList(INewsService newsService)
    {
        _newsService = newsService;
    }
    
    public async Task<IViewComponentResult> InvokeAsync()
    {
        var items = await _newsService.GetLanguageAllAsync(true);
        return View(items);
    }
}