﻿using Microsoft.AspNetCore.Mvc;
using News.WebUI.Services.Abstracts;

namespace News.WebUI.ViewComponents;

public class TopBarNews : ViewComponent
{
    private readonly INewsService _newsService;

    public TopBarNews(INewsService newsService)
    {
        _newsService = newsService;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
        var items = await _newsService.GetByCategoryAsync("63a6200c7a2d2c6ba25a0f40");
        return View(items);
    }
}
