﻿using Microsoft.AspNetCore.Mvc;
using News.WebUI.Services.Abstracts;

namespace News.WebUI.ViewComponents
{
    public class CategoryList : ViewComponent
    {
        private readonly INewsService _newsService;

        public CategoryList(INewsService newsService)
        {
            _newsService = newsService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int selectedView)
        {
            var items = await _newsService.GetCategoryAllAsync();
            ViewBag.sSelectedView = selectedView;
            return View(items);
        }
    }
}
