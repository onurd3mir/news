﻿using Microsoft.AspNetCore.Mvc;
using News.WebUI.Services.Abstracts;

namespace News.WebUI.ViewComponents;

public class TopNewsHeader: ViewComponent
{
    private readonly INewsService _newsService;

    public TopNewsHeader(INewsService newsService)
    {
        _newsService = newsService;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
        var items = await _newsService.GetByCategoryAsync("63a61fe97a2d2c6ba25a0f3e");
        return View(items);
    }
}
