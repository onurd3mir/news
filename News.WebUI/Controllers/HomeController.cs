﻿using Microsoft.AspNetCore.Mvc;
using News.WebUI.Models.Home;
using News.WebUI.Models.Sevices.News;
using News.WebUI.Services.Abstracts;
using System.Diagnostics;
using News.WebUI.Models;

namespace News.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly INewsService _newsService;

        public HomeController(ILogger<HomeController> logger, INewsService newsService)
        {
            _logger = logger;
            _newsService = newsService;
        }

        public async Task<IActionResult> Index()
        {
            var viewModel = new HomeViewModel();
            viewModel.BusinessNews = await _newsService.GetByCategoryAsync("63a61fe97a2d2c6ba25a0f3e");
            viewModel.SportNews = await _newsService.GetByCategoryAsync("63a61fa47a2d2c6ba25a0f3c");
            viewModel.MagazineNews = await _newsService.GetByCategoryAsync("63a6200c7a2d2c6ba25a0f40");
            viewModel.HealthNews = await _newsService.GetByCategoryAsync("63a6201e7a2d2c6ba25a0f42");
            viewModel.ScienceNews = await _newsService.GetByCategoryAsync("63a6203a7a2d2c6ba25a0f44");
            viewModel.TechnologyNews = await _newsService.GetByCategoryAsync("63a6204e7a2d2c6ba25a0f46");

            return View(viewModel);
        }

        [Route("/news/detail/{id}")]
        public async Task<IActionResult> NewsDetail(string id)
        {
            NewsDetailViewModel viewModel = new();
            viewModel.NewsDetail = await _newsService.GetByIdAsync(id);
            return View(viewModel);
        }


        [HttpPost("/news/search")]
        public async Task<IActionResult> NewsSearch(NewsQueryModel newsQuery)
        {
            ViewBag.search = newsQuery.SearchKey + " kelimesine ait bulunan haberler";
            newsQuery.Page = 1;
            newsQuery.PageSize = 20;
            var result = await _newsService.GetQueryAsync(newsQuery);

            return View(result);
        }

        [HttpGet("/news/category/{id}")]
        public async Task<IActionResult> NewsByCategory(string id)
        {
            var newsQuery = new NewsQueryModel
            {
                Page = 1,
                PageSize = 20,
                CategoryId = id,
            };

            var result = await _newsService.GetQueryAsync(newsQuery);

            return View(result);
        }

        [HttpGet("/news/language/{id}")]
        public IActionResult SetLanguage(string id)
        {
            var urlReferrer = Request.Headers["Referer"].ToString();

            const string cookieName = "langcode";
            var getCookie = HttpContext.Request.Cookies[cookieName];

            if (string.IsNullOrEmpty(getCookie)) return Redirect("/");
            HttpContext.Response.Cookies.Delete(cookieName);

            var cookieOptions = new CookieOptions
            {
                Expires = DateTime.Now.AddDays(1)
            };
            HttpContext.Response.Cookies.Append(cookieName, id, cookieOptions);

            return Redirect(urlReferrer.Contains("news/detail") ? "/" : urlReferrer);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}