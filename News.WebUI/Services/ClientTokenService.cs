﻿using IdentityModel.AspNetCore.AccessTokenManagement;
using Microsoft.Extensions.Options;
using News.WebUI.Models;
using News.WebUI.Services.Abstracts;

namespace News.WebUI.Services
{
    public class ClientTokenService : IClientTokenService
    {
        private readonly HttpClient _httpClient;
        private readonly IClientAccessTokenCache _clientAccessTokenCache;
        private readonly ClientSettings _clientSettings;

        public ClientTokenService(HttpClient httpClient, IClientAccessTokenCache clientAccessTokenCache, IOptions<ClientSettings> clientSettings)
        {
            _httpClient = httpClient;
            _clientAccessTokenCache = clientAccessTokenCache;
            _clientSettings = clientSettings.Value;
        }

        public async Task<string> GetToken()
        {
            const string clientToken = "ClientToken";

            var currentToken = await _clientAccessTokenCache.GetAsync(clientToken);

            if (currentToken != null)
            {
                return currentToken.AccessToken!;
            }

            var getToken = await _httpClient.PostAsJsonAsync($"auth", _clientSettings);

            if (getToken.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }

            var newToken = await getToken.Content.ReadFromJsonAsync<AccessTokenModel>();

            var expiresIn = (int)(newToken!.Expiration - DateTime.Now).TotalSeconds;

            await _clientAccessTokenCache.SetAsync(clientToken, newToken.AccessToken, expiresIn);

            return newToken.AccessToken;
        }
    }
}
