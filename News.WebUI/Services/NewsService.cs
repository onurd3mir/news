﻿using News.WebUI.Models.Sevices.News;
using News.WebUI.Services.Abstracts;

namespace News.WebUI.Services;

public class NewsService : INewsService
{
    private readonly HttpClient _httpClient;
    private readonly string _languageCode;

    public NewsService(HttpClient httpClient,IHttpContextAccessor httpContextAccessor)
    {
        _httpClient = httpClient;
        _languageCode = httpContextAccessor.HttpContext?.Request.Cookies["langcode"] ?? "tr";
    }

    public async Task<List<NewsListModel>> GetByCategoryAsync(string categoryId, int page = 1, int pageSize = 10)
    {
        var result = await _httpClient.GetAsync(
            $"news/getbycategory?cayegoryId={categoryId}&languageCode={_languageCode}&page={page}&pageSize={pageSize}");

        if (!result.IsSuccessStatusCode)
        {
            return null;
        }

        return await result.Content.ReadFromJsonAsync<List<NewsListModel>>();
    }

    public async Task<NewsModel> GetByIdAsync(string id)
    {
        var result = await _httpClient.GetAsync($"news/{id}");

        if (!result.IsSuccessStatusCode)
        {
            return null;
        }

        return await result.Content.ReadFromJsonAsync<NewsModel>();
    }

    public async Task<List<CategoryListModel>> GetCategoryAllAsync()
    {
        var result = await _httpClient.GetAsync($"categories");

        if (!result.IsSuccessStatusCode)
        {
            return null;
        }

        return await result.Content.ReadFromJsonAsync<List<CategoryListModel>>();
    }

    public async Task<List<LanguageListModel>> GetLanguageAllAsync(bool isActive)
    {
        var result = await _httpClient.GetAsync($"languages/GetList?isActive={isActive}");

        if (!result.IsSuccessStatusCode)
        {
            return null;
        }

        return await result.Content.ReadFromJsonAsync<List<LanguageListModel>>();
    }

    public async Task<List<NewsListModel>> GetQueryAsync(NewsQueryModel newsQuery)
    {
        newsQuery.LanguageCode = _languageCode;
        var result = await _httpClient.PostAsJsonAsync($"news/Query", newsQuery);

        if (!result.IsSuccessStatusCode)
        {
            return null;
        }

        return await result.Content.ReadFromJsonAsync<List<NewsListModel>>();
    }
}