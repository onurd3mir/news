﻿using News.WebUI.Models.Sevices.News;

namespace News.WebUI.Services.Abstracts;

public interface INewsService
{
    Task<List<NewsListModel>> GetByCategoryAsync(string categoryId, int page = 1, int pageSize = 10);

    Task<NewsModel> GetByIdAsync(string id);

    Task<List<NewsListModel>> GetQueryAsync(NewsQueryModel newsQuery);

    Task<List<CategoryListModel>> GetCategoryAllAsync();
    
    Task<List<LanguageListModel>> GetLanguageAllAsync(bool isActive);
}
