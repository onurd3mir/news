﻿namespace News.WebUI.Services.Abstracts;

public interface IClientTokenService
{
    Task<string> GetToken();
}
