﻿using Microsoft.Extensions.Configuration;
using News.Domain;
using News.Domain.Models.Settings;
using News.Infrastructure.OutSourceServices.Abstracts;
using News.Infrastructure.Persistence.Repositories.Abstracts;
using Quartz;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace News.Producer.AutomaticJobs;

public class CreateNewsJob : IJob
{
    private readonly INewsService _newsService;
    private readonly ICategoryRepository _categoryRepository;
    private readonly IConnection _connection;
    private readonly IModel _channel;

    public CreateNewsJob(INewsService newsService,
        ICategoryRepository categoryRepository,
        IConfiguration configuration)
    {
        var rabbitMqSetting = configuration.GetSection(nameof(RabbitMqSetting)).Get<RabbitMqSetting>();
        var factory = new ConnectionFactory() { HostName = rabbitMqSetting!.Server };
        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
        _channel.ExchangeDeclare(Costants.RabbitMq.CreateNewsExchange, type: ExchangeType.Direct);

        _newsService = newsService;
        _categoryRepository = categoryRepository;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        var properties = _channel.CreateBasicProperties();
        properties.Persistent = true;

        var categories = await _categoryRepository.GetAllAsync();

        foreach (var category in categories.AsParallel())
        {
            var getNews = await _newsService.GetNews("tr", category.Name, "39d88958ff924452af15c15ce7fa9693");

            if (getNews?.Status != "Success")
                continue;

            foreach (var article in getNews.Articles)
            {
                if (string.IsNullOrEmpty(article.Title)
                    || string.IsNullOrEmpty(article.Description)
                    || string.IsNullOrEmpty(article.Content)
                    || string.IsNullOrEmpty(article.UrlToImage)
                    || string.IsNullOrEmpty(article.Url))
                    continue;

                var news = new Domain.Collections.News
                {
                    Source = article.Source.Name,
                    UrlAddress = article.Url,
                    UrlToImageAddress = article.UrlToImage,
                    Title = article.Title,
                    Description = article.Description,
                    Content = article.Content,
                    PublishedAt = article.PublishedAt,
                    LangCode = "tr",
                    Category = category
                };

                var data = JsonSerializer.Serialize(news, new JsonSerializerOptions { WriteIndented = true });
                var bytemessage = Encoding.UTF8.GetBytes(data);
                _channel.BasicPublish(
                    exchange: Costants.RabbitMq.CreateNewsExchange,
                    routingKey: Costants.RabbitMq.CreateNews,
                    basicProperties: properties,
                    body: bytemessage);

                Console.WriteLine(data + "\n\n");
            }
        }

        _channel.Dispose();
        _connection.Dispose();
    }
}