﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using News.Infrastructure;
using News.Producer.AutomaticJobs;
using Quartz;

using var host = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration(app =>
    {
        app.AddJsonFile("appsettings.json");
    })
    .ConfigureServices((_, services) =>
        services
        .AddQuartz(q =>
        {
            q.UseMicrosoftDependencyInjectionScopedJobFactory();

            var createNewsJob = nameof(CreateNewsJob).ToLower();
            q.AddJob<CreateNewsJob>(opts => opts.WithIdentity(createNewsJob));
            q.AddTrigger(opts => opts
                .ForJob(createNewsJob)
                .WithIdentity(createNewsJob + "-trigger")
                .StartAt(DateBuilder.FutureDate(1, IntervalUnit.Minute))
                .WithSimpleSchedule(x => x
                    .WithIntervalInMinutes(3)
                    .RepeatForever())
            );
        })
        .AddQuartzHostedService(q => q.WaitForJobsToComplete = true)
        .AddInfrastructureService(_.Configuration))
    .Build();

await host.RunAsync();



