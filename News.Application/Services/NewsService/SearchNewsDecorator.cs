﻿using News.Infrastructure.CrossCuttingConcerns.Searching;

namespace News.Application.Services.NewsService;

public class SearchNewsDecorator : INewsService
{
    private readonly INewsService _decorator;
    private readonly IElasticSearch _elasticSearch;

    public SearchNewsDecorator(INewsService decorator, IElasticSearch elasticSearch)
    {
        _decorator = decorator;
        _elasticSearch = elasticSearch;
    }

    public async Task<List<Domain.Collections.News>> GetByCategoryQueryAsync(string languageCode, string categoryId, int page, int pageSize)
    {
        var news = await _elasticSearch.Client.SearchAsync<Domain.Collections.News>(s =>
                                    s.Index(Domain.Costants.ElasticSearch.NewsApiIndexAliasName)
                                    .From((page - 1) * pageSize)
                                    .Size(pageSize)
                                    .Query(q =>
                                           q.Bool(b =>
                                                  b.Must(m => m.Term(f => f.Category.Id, categoryId),
                                                         m => m.Term(f => f.LangCode, languageCode)
                                                         )

                                                 )
                                           )
                                    .Sort(x => x.Descending(f => f.PublishedAt)));

        return news.Documents.ToList();
    }

    public async Task<Domain.Collections.News> GetByIdAsync(string id)
    {
        return await _decorator.GetByIdAsync(id);
    }

    public async Task<List<Domain.Collections.News>> GetListNewsQueryAsync(string languageCode, string categoryId, string searchKey, int page, int pageSize)
    {
        var news = await _elasticSearch.Client.SearchAsync<Domain.Collections.News>(s =>
                                    s.Index(Domain.Costants.ElasticSearch.NewsApiIndexAliasName)
                                    .From((page - 1) * pageSize)
                                    .Size(pageSize)
                                    .Query(q =>
                                           q.Bool(b =>
                                                  b.Must(m =>
                                                        {
                                                            if (!string.IsNullOrEmpty(categoryId))
                                                                m.Term(f => f.Category.Id, categoryId);

                                                            return m;
                                                        },
                                                         m =>
                                                         {
                                                             if (!string.IsNullOrEmpty(languageCode))
                                                                 m.Term(f => f.LangCode, languageCode);

                                                             return m;
                                                         },
                                                         m => m.Bool(b =>
                                                                     b.Should(
                                                                        s =>
                                                                        {
                                                                            if (!string.IsNullOrEmpty(searchKey))
                                                                                s.MatchPhrasePrefix(mp => mp.Field(f => f.Title).Query(searchKey));

                                                                            return s;
                                                                        },
                                                                        s =>
                                                                        {
                                                                            if (!string.IsNullOrEmpty(searchKey))
                                                                                s.MatchPhrasePrefix(mp => mp.Field(f => f.Description).Query(searchKey));

                                                                            return s;
                                                                        })
                                                                     )
                                                         )
                                                )
                                           )
                                    .Sort(x => x.Descending(f => f.PublishedAt)));

        return news.Documents.ToList();
    }
}
