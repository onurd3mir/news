﻿using News.Infrastructure.CrossCuttingConcerns.Caching;

namespace News.Application.Services.NewsService;

public class CacheNewsDecorator : INewsService
{
    private readonly INewsService _decorator;
    private readonly ICacheService _cacheService;

    public CacheNewsDecorator(INewsService decorator, ICacheService cacheService)
    {
        _decorator = decorator;
        _cacheService = cacheService;
    }

    public async Task<List<Domain.Collections.News>> GetByCategoryQueryAsync(string languageCode, string categoryId, int page, int pageSize)
    {
        return await _decorator.GetByCategoryQueryAsync(languageCode, categoryId, page, pageSize);
    }

    public async Task<Domain.Collections.News> GetByIdAsync(string id)
    {
        var key = "NewsGetById_" + id;

        var result = await _cacheService.GetAsync(key,
            10,
            () => _decorator.GetByIdAsync(id).Result);

        return result;
    }

    public async Task<List<Domain.Collections.News>> GetListNewsQueryAsync(string languageCode, string categoryId, string searchKey, int page, int pageSize)
    {
        return await _decorator.GetListNewsQueryAsync(languageCode, categoryId, searchKey, page, pageSize);
    }
}
