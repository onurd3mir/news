﻿namespace News.Application.Services.NewsService;

public interface INewsService
{
    Task<Domain.Collections.News> GetByIdAsync(string id);
    Task<List<Domain.Collections.News>> GetListNewsQueryAsync(string languageCode, string categoryId, string searchKey, int page, int pageSize);
    Task<List<Domain.Collections.News>> GetByCategoryQueryAsync(string languageCode, string categoryId, int page, int pageSize);
}
