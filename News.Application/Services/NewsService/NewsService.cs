﻿using News.Infrastructure.Helpers;
using News.Infrastructure.Persistence.Repositories.Abstracts;

namespace News.Application.Services.NewsService;

public class NewsService : INewsService
{
    private readonly INewsRepository _newsRepository;

    public NewsService(INewsRepository newsRepository)
    {
        _newsRepository = newsRepository;
    }

    public async Task<List<Domain.Collections.News>> GetByCategoryQueryAsync(string languageCode, string categoryId, int page, int pageSize)
    {
        return await _newsRepository.GetByCategoryAsync(categoryId, languageCode, page, pageSize);
    }

    public async Task<Domain.Collections.News> GetByIdAsync(string id)
    {
        return await _newsRepository.GetByIdAsync(id);
    }

    public async Task<List<Domain.Collections.News>> GetListNewsQueryAsync(string languageCode, string categoryId, string searchKey, int page, int pageSize)
    {
        var predicate = PredicateBuilder.True<Domain.Collections.News>();

        if (!string.IsNullOrEmpty(categoryId))
            predicate = predicate.And(i => i.Category.Id == categoryId);

        if (!string.IsNullOrEmpty(searchKey))
            predicate = predicate.And(i => i.Title.ToLower().Contains(searchKey.ToLower())
                                           || i.Description.ToLower().Contains(searchKey.ToLower()));

        if (!string.IsNullOrEmpty(languageCode))
            predicate = predicate.And(i => i.LangCode == languageCode);

        return await _newsRepository.GetFilterAsync(predicate, page, pageSize);
    }
}
