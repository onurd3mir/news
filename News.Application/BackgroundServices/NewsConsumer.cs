﻿using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using News.Domain;
using News.Domain.Models.Settings;
using News.Infrastructure.Persistence.Repositories;
using News.Infrastructure.Persistence.Repositories.Abstracts;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace News.Application.BackgroundServices;

public class NewsConsumer : BackgroundService
{
    private readonly INewsRepository _newsRepository;
    private readonly IConnection _connection;
    private readonly IModel _channel;
    public NewsConsumer(IConfiguration configuration)
    {
        var rabbitMqSetting = configuration.GetSection(nameof(RabbitMqSetting)).Get<RabbitMqSetting>();
        var factory = new ConnectionFactory() { HostName = rabbitMqSetting!.Server };
        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
        _channel.ExchangeDeclare(Costants.RabbitMq.CreateNewsExchange, type: ExchangeType.Direct);

        _newsRepository = new NewsRepository(configuration);
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var queueName = _channel.QueueDeclare().QueueName;
        _channel.QueueBind(queue: queueName, exchange: Costants.RabbitMq.CreateNewsExchange, routingKey: Costants.RabbitMq.CreateNews);
        var consumer = new EventingBasicConsumer(_channel);

        consumer.Received += (sender, e) =>
        {
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            var routingKey = e.RoutingKey;
            Console.WriteLine(message);
            _channel.BasicAck(e.DeliveryTag, false);

            var article = JsonSerializer.Deserialize<Domain.Collections.News>(message);

            if (!_newsRepository.AnyByUrl(article.UrlAddress).Result)
            {
                _newsRepository.AddAsync(article);
            }
        };

        _channel.BasicConsume(queueName, false, consumer);

        return Task.CompletedTask;
    }

    public override Task StopAsync(CancellationToken cancellationToken)
    {
        _channel.Dispose();
        _connection.Dispose();

        return base.StopAsync(cancellationToken);
    }
}