﻿using System.Reflection;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using News.Application.AutomaticJobs;
using News.Application.BackgroundServices;
using News.Application.Behaviors;
using News.Application.Features.Users.Jwt;
using News.Application.Services.NewsService;
using News.Infrastructure;

namespace News.Application;

public static class ApplicationServiceRegistration
{
    public static void AddApplicationService(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddMediatR(Assembly.GetExecutingAssembly());
        services.AddInfrastructureService(configuration);
        services.AddAJobService();
        //services.AddHostedService<NewsConsumer>();

        services.AddScoped(typeof(IPipelineBehavior<,>), typeof(LoggingPipelineBehavior<,>));
        services.AddScoped<IJwtProvider, JwtProvider>();
        services.AddScoped<INewsService, NewsService>();

        //services.Decorate<INewsService, CacheNewsDecorator>().
        //    Decorate<INewsService, SearchNewsDecorator>();

        services.Decorate<INewsService, CacheNewsDecorator>();
    }
}