﻿using Microsoft.Extensions.Logging;
using News.Infrastructure.CrossCuttingConcerns.Searching;
using News.Infrastructure.OutSourceServices.Abstracts;
using News.Infrastructure.Persistence.Repositories.Abstracts;
using Quartz;

namespace News.Application.AutomaticJobs;

internal class CreateNewsJob : IJob
{
    private readonly INewsService _newsService;
    private readonly INewsRepository _newsRepository;
    private readonly ICategoryRepository _categoryRepository;
    private readonly ILanguageRepository _languageRepository;
    private readonly ILogger<CreateNewsJob> _logger;
    private readonly IElasticSearch _elasticSearch;

    public CreateNewsJob(INewsService newsService,
        INewsRepository newsRepository,
        ICategoryRepository categoryRepository,
        ILanguageRepository languageRepository,
        ILogger<CreateNewsJob> logger,
        IElasticSearch elasticSearch)
    {
        _newsService = newsService;
        _newsRepository = newsRepository;
        _categoryRepository = categoryRepository;
        _languageRepository = languageRepository;
        _logger = logger;
        _elasticSearch = elasticSearch;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        var languages = await _languageRepository.GetAllAsync();
        var categories = await _categoryRepository.GetAllAsync();

        await _elasticSearch.CreateIndexAsync(new Domain.ElasticSearch.ElasticSearchModel
        {
            IndexName = Domain.Costants.ElasticSearch.NewsApiIndexName,
            AliasName = Domain.Costants.ElasticSearch.NewsApiIndexAliasName
        });

        foreach (var lang in languages.AsParallel())
        {
            foreach (var category in categories.AsParallel())
            {
                _logger.LogInformation($"{lang.Name} - {category.Name} Haberleri Çekiliyor...");

                var getNews = await _newsService.GetNews(lang.Code, category.Name, "39d88958ff924452af15c15ce7fa9693");

                if (getNews?.Status != "Success") return;

                var articles = getNews.Articles;

                foreach (var article in articles.AsParallel())
                {
                    if (string.IsNullOrEmpty(article.Title)
                        || string.IsNullOrEmpty(article.Description)
                        || string.IsNullOrEmpty(article.Content)
                        || string.IsNullOrEmpty(article.UrlToImage)
                        || string.IsNullOrEmpty(article.Url))
                        continue;

                    if (await _newsRepository.AnyByUrl(article.Url))
                        continue;

                    var newsModel = new Domain.Collections.News
                    {
                        Source = article.Source.Name,
                        UrlAddress = article.Url,
                        UrlToImageAddress = article.UrlToImage,
                        Title = article.Title,
                        Description = article.Description,
                        Content = article.Content,
                        PublishedAt = article.PublishedAt,
                        LangCode = lang.Code,
                        Category = category
                    };

                    await _newsRepository.AddAsync(newsModel);

                    await _elasticSearch.InsertAsync(new Domain.ElasticSearch.ElasticSearchInsertModel
                    {
                        IndexName = Domain.Costants.ElasticSearch.NewsApiIndexName,
                        AliasName = Domain.Costants.ElasticSearch.NewsApiIndexAliasName,
                        Item = newsModel
                    });
                }
            }
        }
    }
}