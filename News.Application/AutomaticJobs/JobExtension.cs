﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace News.Application.AutomaticJobs;

public static class JobExtension
{
    public static void AddAJobService(this IServiceCollection services)
    {
        services.AddQuartz(q =>
        {
            q.UseMicrosoftDependencyInjectionScopedJobFactory();
            
            q.AddJob<CreateNewsJob>(opts => opts.WithIdentity(nameof(CreateNewsJob).ToLower()));
            q.AddTrigger(opts => opts
                .ForJob(nameof(CreateNewsJob).ToLower())
                .WithIdentity(nameof(CreateNewsJob).ToLower() + "-trigger")
                //.WithCronSchedule("0 0/1 * * * ?")
                .StartAt(DateBuilder.FutureDate(1, IntervalUnit.Minute))
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(1)
                    .WithRepeatCount(5))
            );
        });
        services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);
    }
}