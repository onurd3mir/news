﻿using MediatR;
using News.Application.Features.Languages.Dtos;
using News.Domain.Models;
using News.Infrastructure.Persistence.Repositories.Abstracts;

namespace News.Application.Features.Languages.Queries;

public class GetListLanguageQuery : IRequest<ResponseModel<List<LanguageListDto>>>
{
    public bool IsActive { get; set; }

    public class
        GetListLanguageQueryHandler : IRequestHandler<GetListLanguageQuery, ResponseModel<List<LanguageListDto>>>
    {
        private readonly ILanguageRepository _languageRepository;

        public GetListLanguageQueryHandler(ILanguageRepository languageRepository)
        {
            _languageRepository = languageRepository;
        }

        public async Task<ResponseModel<List<LanguageListDto>>> Handle(GetListLanguageQuery request,
            CancellationToken cancellationToken)
        {
            var languages = await _languageRepository.GetListQueryAsync(request.IsActive);

            var result = languages.Select(p => new LanguageListDto
            {
                Name = p.Name,
                Code = p.Code
            }).ToList();

            return ResponseModel<List<LanguageListDto>>.Success(result, "");
        }
    }
}