﻿namespace News.Application.Features.Languages.Dtos;

public class LanguageListDto
{
    public string Code { get; set; }
    
    public string Name { get; set; }
}