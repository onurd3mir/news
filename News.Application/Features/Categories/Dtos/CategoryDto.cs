﻿namespace News.Application.Features.Categories.Dtos;

public class CategoryDto
{
    public string CategoryId { get; set; }
    public string CategoryName { get; set; }
}