﻿using MediatR;
using News.Application.Features.Categories.Dtos;
using News.Domain.Models;
using News.Infrastructure.CrossCuttingConcerns.Caching;
using News.Infrastructure.Persistence.Repositories.Abstracts;

namespace News.Application.Features.Categories.Queries;

public class GetListCategoryQuery : IRequest<ResponseModel<List<CategoryDto>>>
{
    public class GetListCategoryQueryHandler : IRequestHandler<GetListCategoryQuery, ResponseModel<List<CategoryDto>>>
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICacheService _cacheService;

        public GetListCategoryQueryHandler(ICategoryRepository categoryRepository, ICacheService cacheService)
        {
            _categoryRepository = categoryRepository;
            _cacheService = cacheService;
        }

        public async Task<ResponseModel<List<CategoryDto>>> Handle(GetListCategoryQuery request,
            CancellationToken cancellationToken)
        {
            var categoryList = await _cacheService.GetAsync(
                "categoriesGetAll",
                60,
                () => _categoryRepository.GetAllAsync().Result);

            var categoryListDto = categoryList.Select(p => new CategoryDto
            {
                CategoryId = p.Id,
                CategoryName = p.Name
            }).ToList();

            return ResponseModel<List<CategoryDto>>.Success(categoryListDto, "");
        }
    }
}