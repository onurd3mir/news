﻿using MediatR;
using News.Application.Features.News.Dtos;
using News.Application.Services.NewsService;
using News.Domain.Models;

namespace News.Application.Features.News.Queries;

public class GetByIdNewsQuery : IRequest<ResponseModel<NewsDto>>
{
    public string Id { get; set; }

    public class GetByIdNewsQueryHandler : IRequestHandler<GetByIdNewsQuery, ResponseModel<NewsDto>>
    {
        private readonly INewsService _newsService;

        public GetByIdNewsQueryHandler(INewsService newsService)
        {
            _newsService = newsService;
        }

        public async Task<ResponseModel<NewsDto>> Handle(GetByIdNewsQuery request, CancellationToken cancellationToken)
        {
            var news = await _newsService.GetByIdAsync(request.Id);

            if (news is null)
                return ResponseModel<NewsDto>.Fail("Kayıt Bulunamadı");

            var newsDto = new NewsDto
            {
                NewsId = news.Id,
                Title = news.Title,
                Description = news.Description,
                Content = news.Content,
                PublishedAt = news.PublishedAt,
                UrlAddress = news.UrlAddress,
                UrlToImageAddress = news.UrlToImageAddress,
                Source = news.Source,
                CategoryName = news.Category.Name,
                LangCode = news.LangCode
            };

            return ResponseModel<NewsDto>.Success(newsDto, "");
        }
    }
}