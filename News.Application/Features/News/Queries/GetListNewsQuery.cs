﻿using MediatR;
using News.Application.Features.News.Dtos;
using News.Application.Services.NewsService;
using News.Domain.Models;

namespace News.Application.Features.News.Queries;

public class GetListNewsQuery : IRequest<ResponseModel<List<NewsListDto>>>
{
    public string LanguageCode { get; set; }
    public string CategoryId { get; set; }
    public string SearchKey { get; set; }
    public int Page { get; set; } = 1;
    public int PageSize { get; set; } = 10;

    public class GetListNewsQueryHandler : IRequestHandler<GetListNewsQuery, ResponseModel<List<NewsListDto>>>
    {
        private readonly INewsService _newsService;


        public GetListNewsQueryHandler(INewsService newsService)
        {
            _newsService = newsService;
        }

        public async Task<ResponseModel<List<NewsListDto>>> Handle(GetListNewsQuery request,
            CancellationToken cancellationToken)
        {
            var news = await _newsService.GetListNewsQueryAsync(request.LanguageCode, request.CategoryId, request.SearchKey, request.Page, request.PageSize);

            var result = news.Select(p => new NewsListDto
            {
                NewsId = p.Id,
                Title = p.Title,
                Content = p.Content,
                Description = p.Description,
                PublishedAt = p.PublishedAt,
                CategoryName = p.Category.Name,
                UrlAddress = p.UrlAddress,
                UrlToImageAddress = p.UrlToImageAddress,
                LangCode = p.LangCode,
                Source = p.Source
            }).ToList();

            return ResponseModel<List<NewsListDto>>.Success(result, "");
        }
    }
}