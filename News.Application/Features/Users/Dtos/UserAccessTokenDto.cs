﻿namespace News.Application.Features.Users.Dtos;

public class UserAccessTokenDto
{
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
    public DateTime Expiration { get; set; }
}
