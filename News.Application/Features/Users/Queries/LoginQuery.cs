﻿using MediatR;
using News.Application.Features.Users.Dtos;
using News.Application.Features.Users.Jwt;
using News.Domain.Models;

namespace News.Application.Features.Users.Queries;

public class LoginQuery : IRequest<ResponseModel<UserAccessTokenDto>>
{
    public string Email { get; set; }
    public string Password { get; set; }

    public class LoginQueryHandler : IRequestHandler<LoginQuery, ResponseModel<UserAccessTokenDto>>
    {
        private readonly IJwtProvider _jwtProvider;

        public LoginQueryHandler(IJwtProvider jwtProvider)
        {
            _jwtProvider = jwtProvider;
        }

        public async Task<ResponseModel<UserAccessTokenDto>> Handle(LoginQuery request, CancellationToken cancellationToken)
        {
            await Task.Delay(300);

            if (!request.Email.Equals("onur1997onur@gmail.com") || !request.Password.Equals("1234"))
            {
                return ResponseModel<UserAccessTokenDto>.Fail("Giriş Başarısız");
            }

            var userAccessToken = _jwtProvider.Generate();

            return ResponseModel<UserAccessTokenDto>.Success(userAccessToken, "Giriş Başarılı");
        }
    }
}
