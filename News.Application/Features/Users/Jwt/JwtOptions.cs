﻿namespace News.Application.Features.Users.Jwt;

public class JwtOptions
{
    public string? Issuer { get; set; }
    public string? Audience { get; set; }
    public string? SecretKey { get; set; }
    public int AccessTokenExpiration { get; set; }
}
