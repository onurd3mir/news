﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using News.Application.Features.Users.Dtos;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace News.Application.Features.Users.Jwt;

public class JwtProvider : IJwtProvider
{
    private readonly JwtOptions _options;

    public JwtProvider(IOptions<JwtOptions> options)
    {
        _options = options.Value;
    }

    public UserAccessTokenDto Generate()
    {
        var claims = new Claim[] { };

        var signingCredentials = new SigningCredentials(
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.SecretKey)),
            SecurityAlgorithms.HmacSha256);

        var expires = DateTime.Now.AddHours(_options.AccessTokenExpiration);

        var token = new JwtSecurityToken(
            _options.Issuer,
            _options.Audience,
            claims,
            null,
            expires,
            signingCredentials);

        var tokenValue = new JwtSecurityTokenHandler()
            .WriteToken(token);

        return new UserAccessTokenDto
        {
            AccessToken = tokenValue,
            RefreshToken = string.Empty,
            Expiration = expires
        };
    }
}
