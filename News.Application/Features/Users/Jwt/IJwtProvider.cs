﻿using News.Application.Features.Users.Dtos;

namespace News.Application.Features.Users.Jwt;

public interface IJwtProvider
{
    public UserAccessTokenDto Generate();
}
