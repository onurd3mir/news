﻿using News.Infrastructure.Persistence.Repositories.Abstracts;

namespace News.WebApi;

public static class SeedData
{
    public static void Seed(this WebApplication app)
    {
        using var scope = app.Services.CreateScope();

        var categoryRepository = scope.ServiceProvider.GetRequiredService<ICategoryRepository>();

        var cagtegories = categoryRepository.GetAllAsync().Result;

        if (!cagtegories.Any())
        {
            categoryRepository.AddAsync(new Domain.Collections.Category { Name = "Spor" }).Wait();
            categoryRepository.AddAsync(new Domain.Collections.Category { Name = "İş" }).Wait();
            categoryRepository.AddAsync(new Domain.Collections.Category { Name = "Magazin" }).Wait();
            categoryRepository.AddAsync(new Domain.Collections.Category { Name = "Sağlık" }).Wait();
            categoryRepository.AddAsync(new Domain.Collections.Category { Name = "Bilim" }).Wait();
            categoryRepository.AddAsync(new Domain.Collections.Category { Name = "Teknoloji" }).Wait();
        }

        var langueageRepository = scope.ServiceProvider.GetRequiredService<ILanguageRepository>();

        var languages = langueageRepository.GetAllAsync().Result;

        if (!languages.Any())
        {
            langueageRepository.AddAsync(new Domain.Collections.Language
            {
                Code = "tr",
                IsActive = true,
                Name = "Türkçe"
            }).Wait();
        }

        var newsRepository = scope.ServiceProvider.GetRequiredService<INewsRepository>();

        var news = newsRepository.GetAllAsync().Result;

        if (!news.Any())
        {
            var category = categoryRepository.GetAllAsync().Result.First();

            newsRepository.AddAsync(new Domain.Collections.News
            {
                Category = category,
                Content = string.Empty,
                Description = string.Empty,
                LangCode = "tr",
                PublishedAt = DateTime.Now,
            }).Wait();
        }
    }
}
