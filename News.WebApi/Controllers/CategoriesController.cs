using Microsoft.AspNetCore.Mvc;
using News.Application.Features.Categories.Queries;

namespace News.WebApi.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoriesController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult> GetList()
        {
            var newsList = await Mediator!.Send(new GetListCategoryQuery());

            if (newsList.Status is false)
                return BadRequest(newsList.Message);

            return Ok(newsList.Result);
        }
    }
}