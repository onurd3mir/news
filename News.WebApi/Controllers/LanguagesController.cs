﻿using Microsoft.AspNetCore.Mvc;
using News.Application.Features.Languages.Queries;

namespace News.WebApi.Controllers;

[Route("api/languages")]
[ApiController]
public class LanguagesController : BaseController
{
    [HttpGet, Route("GetList")]
    public async Task<ActionResult> GetList(bool isActive)
    {
        var newsList = await Mediator!.Send(new GetListLanguageQuery { IsActive = isActive });

        if (newsList.Status is false)
            return BadRequest(newsList.Message);

        return Ok(newsList.Result);
    }
}