﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using News.Application.Features.Users.Queries;

namespace News.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : BaseController
    {
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginQuery loginQuery)
        {
            var authorize = await Mediator!.Send(loginQuery);

            if (!authorize.Status)
                return Unauthorized(authorize.Message);

            return Ok(authorize.Result);
        }
    }
}
