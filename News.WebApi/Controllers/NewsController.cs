﻿using Microsoft.AspNetCore.Mvc;
using News.Application.Features.News.Queries;

namespace News.WebApi.Controllers;

[ApiController]
[Route("api/news")]
public class NewsController : BaseController
{
    [HttpPost("Query")]
    public async Task<ActionResult> ListQuery(GetListNewsQuery query)
    {
        var newsList = await Mediator!.Send(query);

        if (newsList.Status is false)
            return BadRequest(newsList.Message);

        return Ok(newsList.Result);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult> Get(string id)
    {
        var news = await Mediator!.Send(new GetByIdNewsQuery { Id = id });

        if (news.Status is false)
            return BadRequest(news.Message);

        return Ok(news.Result);
    }

    [HttpGet("GetByCategory")]
    public async Task<ActionResult> GetByCategory(string cayegoryId, string languageCode, int page = 1,
        int pageSize = 10)
    {
        var news = await Mediator!.Send(new GetByCategoryQuery
            { CategoryId = cayegoryId, LanguageCode = languageCode, Page = page, PageSize = pageSize });

        if (news.Status is false)
            return BadRequest(news.Message);

        return Ok(news.Result);
    }
}