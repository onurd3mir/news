FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app


FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["News.WebApi/News.WebApi.csproj", "News.WebApi/"]
COPY ["News.Application/News.Application.csproj", "News.Application/"]
COPY ["News.Infrastructure/News.Infrastructure.csproj", "News.Infrastructure/"]
COPY ["News.Domain/News.Domain.csproj", "News.Domain/"]
RUN dotnet restore "News.WebApi/News.WebApi.csproj"
COPY . .
WORKDIR "/src/News.WebApi"
RUN dotnet build "News.WebApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "News.WebApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "News.WebApi.dll"]